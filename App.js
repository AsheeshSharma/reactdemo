/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar 
} from 'react-native';
import AppNavigator from './src/AppNavigator';
import { colors } from './src/utils';
import stores from './src/stores'
import { Provider } from 'mobx-react'

export default class App extends Component<{}> {
  render() {
    return (
      <Provider {...stores} >
      <View style={styles.container}>
        <StatusBar
           backgroundColor={colors.YellowTheme}
           barStyle="light-content"
         />
        <AppNavigator/>
      </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    flex: 1,
  }
});
