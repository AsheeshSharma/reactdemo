import { icons } from './icons'

export const COUNTRY_CODE_DATA = [
    {
      image: icons.COUNTRY_FLAG_ICON_DUBAI,
      countryCode: '+971',
      countryName: 'United Arab Emirates'
    }, {
      image: icons.COUNTRY_FLAG_ICON_UAE,
      countryCode: '+966',
      countryName: 'Soudi Arabia'
    }, {
      image: icons.COUNTRY_FLAG_ICON_KUWAIT,
      countryCode: '+965',
      countryName: 'Kuwait'
    }
  ]

export  const OTHER_CODE_DATA = [
    {
        code: '+55'
    }, {
        code: '+56'
    }, {
        code: '+57'
    }
]