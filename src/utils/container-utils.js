import React from 'react'
import { BaseContainer } from '../BaseContainer'
import PropTypes from 'prop-types'

export function getContainable(
    C: React.ComponentClass | React.SFC,
    configuration: {} = {},
    internalProps: {} = {}
): any {

    class WrappedComponent extends React.PureComponent {
        render() {
            return <BaseContainer configuration={configuration} {...this.props} onForceCoachMarkStart={() => this.onForceCoachMarkStart()}>
                <C {...internalProps} {...this.props} />
            </BaseContainer>
        }
    }
    return WrappedComponent
}