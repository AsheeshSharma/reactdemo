export const icons = {
    COUNTRY_FLAG_ICON_DUBAI: require('../images/germany.png'),
    COUNTRY_FLAG_ICON_UAE: require('../images/eu.png'),
    COUNTRY_FLAG_ICON_KUWAIT: require('../images/spain.png'),
    NOTIFICATION_ICON: require('../images/notification.png'),
    BACK_ICON: require('../images/left_arrow.png')
}
