import { Platform } from 'react-native'

const roundedButton = {
    backgroundColor: 'white',
    borderRadius: 5,
    width: 100,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
}

const colors = {
    BorderColor: '#DDD',
    grayText: '#A09D97',
    YellowTheme: '#FBE858',
    CharcoalBlack: '#071323',
    BlueDarkAppTheme: '#4792B9',
    SlatyGrey: '#939da0',
    grayBackground: '#F9FAF9',
    grayTitleTabs: '#94928C',
    LightBlack: '#0f1f2f',
    FadeBlue: '#70acd3',
    Orange: '#FBA400',
    LightRed: '#c52834',
    BasicColor: '#ff7058',
    DeepOrange: '#f4a119',
    DeepShadedBlue: '#5682a3',
    LightBlue: '#18AFD6',
    DeepBlue: '#396bd4',
    ShadowColor: '#000',
    SkyBlueLight: '#71abe0',
    IndicatorColor: '#EB7561',
    WhiteGrayish: '#F6F6F6',
    LightGrey: '#999999',
    DarkGrey: '#1a1a1a',
    White: '#FFFFFF',
    GrayCalm: '#FBFBFB',
    ButtonGray: '#404552',
    GrayTitle: '#F5F5F5',
    GrayUnderLine: '#D2D2D2',
    Yellow: '#FFFF00',
    Fuchsia: '#FF00FF',
    Red: '#FF0000',
    Silver: '#C0C0C0',
    Olive: '#808000',
    Purple: '#800080',
    Maroon: '#800000',
    Aqua: '#00FFFF',
    Lime: '#00FF00',
    Teal: '#008080',
    Green: '#008000',
    Blue: '#0000FF',
    Navy: '#000080',
    Black: '#000000',
    Ivory: '#FFFFF0',
    DarkGreen: '#006400',
    MediumBlue: '#0000CD',
    DarkBlue: '#00008B',
    ColorUltimate: '#58afc4',
    ColorRankBooster: '#228b22',
    ColorBasic: '#ef8644',
    ColorPremimum: '#bf6061',
    Transparent: '#00000000',
    SkyeBlue: '#2e93fb',
    AccentBlue: '#2e93fb',
    MettalicGrey: '#686868',
    LightGrayOpacity50: '#F1F1F1',
    LightGrayOpacity30: '#fafafa',
    WhiteColorOpacity75: 'rgba(0, 0, 0, 0.2)',
    WhiteColorOpacity60: 'rgba(255, 255, 255, 0.6)',
    BlackColorOpacity05: '#0b000000',
    BlackColorOpacity10: '#1a000000',
    BlackColorOpacity20: '#24000000',
    BlackColorOpacity25: '#33000000',
    BlackColorOpacity30: '#4d000000',
    BlackColorOpacity35: '#66000000',
    BlackColorOpacity40: '#80000000',
    BlackColorOpacity45: '#8c000000',
    BlackColorOpacity50: '#99000000',
    BlackColorOpacity60: '#b3000000',
    BlackColorOpacity70: '#cc000000',
    BlackColorOpacity80: '#d9000000',
    BlackColorOpacity90: '#e6000000',
    PrimaryBlue: '#2e93fb',
    DrawerBlue: '#03202E',
    DrawerTextBlue: '#7C93A0',
    DrawerHighlightedBlue: '#03344c',
    GraphOrange: '#F9A443',
    GraphGreen: '#6FCB0A',
    Gray: '#808080',
    ProgressBarGray: '#DCDCDC',
    ProgressBarOrange: '#f6a623',
    ProgressBarBlue: '#4a90e2',
    ProgressBarYellow: '#f8e71c',
    SummaryContent: '#FFFFFF',
    fillColor: '#FFFFFF'
}

const theme = {

    fontSizeBase: 15,
    fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue' : 'Roboto',
    fontFamilyThin: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'Roboto',
    fontFamilyLight: (Platform.OS === 'ios') ? 'HelveticaNeue-Light' : 'Roboto',

    btnFontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue' : 'Roboto_medium',

    titleFontSize: (Platform.OS === 'ios') ? 17 : 19,
    subTitleFontSize: (Platform.OS === 'ios') ? 12 : 14,

    get fontSizeH1() {
        return this.fontSizeBase * 1.8
    },
    get fontSizeH2() {
        return this.fontSizeBase * 1.6
    },
    get fontSizeH3() {
        return this.fontSizeBase * 1.4
    },
    get btnTextSize() {
        return (Platform.OS === 'ios') ? this.fontSizeBase * 1.1 :
            this.fontSizeBase - 1
    },
    get btnTextSizeLarge() {
        return this.fontSizeBase * 1.5
    },
    get btnTextSizeSmall() {
        return this.fontSizeBase * .8
    },
    get iconSizeLarge() {
        return this.iconFontSize * 1.5
    },
    get iconSizeSmall() {
        return this.iconFontSize * .6
    },

    get borderRadiusLarge() {
        return this.fontSizeBase * 3.8
    },

    toolbarHeight: (Platform.OS === 'ios') ? 64 : 56,
    radioBtnSize: (Platform.OS === 'ios') ? 25 : 23,
    iconFontSize: (Platform.OS === 'ios') ? 30 : 28,
    lineHeight: (Platform.OS === 'ios') ? 20 : 24,
    iconLineHeight: (Platform.OS === 'ios') ? 37 : 30,
    toolbarIconSize: (Platform.OS === 'ios') ? 20 : 22
}

const dimens = {
    size0: 0, size1: 1, size2: 2, size3: 3, size4: 4, size5: 5, size6: 6, size7: 7, size8: 8,
    size9: 9, size10: 10, size11: 11, size12: 12, size13: 13, size14: 14, size15: 15,
    size16: 16, size17: 17, size18: 18, size19: 19, size20: 20, size21: 21, size22: 22,
    size23: 23, size24: 24, size25: 25, size26: 26, size27: 27, size28: 28, size29: 29,
    size30: 30, size31: 31, size32: 32, size33: 33, size34: 34, size35: 35, size36: 36,
    size38: 38, size39: 39, size40: 40, size41: 40, size42: 42, size43: 43, size44: 44,
    size45: 45, size46: 46, size47: 47, size48: 48, size49: 49, size50: 50, size53: 53,
    size58: 58, size60: 60, size65: 65, size70: 70, size75: 75, size80: 80
}

export { colors, theme, dimens, roundedButton }
