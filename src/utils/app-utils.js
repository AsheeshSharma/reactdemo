import Snackbar from 'react-native-snackbar';

export function showSnackbar(title) {
    Snackbar.show({
        title: title,
        duration: Snackbar.LENGTH_SHORT,
      });
}