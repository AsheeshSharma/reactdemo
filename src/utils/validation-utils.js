export function isUserDataValid({ phone, email, countryCode, otherCode, password, isTCChecked }) {
    if (!isPhoneValid(countryCode, otherCode, phone)) {
        return {valid : false, message : "Phone is invalid. Try again."}
    }
    if (!isPasswordValid(password)) {
        return {valid : false, message : 'Password is invalid. Try again.'}
    }
    if (!isEmailValid(email)) {
        return {valid : false, message : 'Email is invalid. Try again.'}
    }
    if (!isTCChecked) {
        return {valid : false, message : 'Please accept the Terms and Conditions.'}
    }

    return {valid : true, message : 'Processing Information, OTP Request Sent'}
}

export function isPhoneValid(countryCode, otherCode, phone) {
    if (!countryCode || countryCode.length !== 4 || countryCode === '+XXX') {
        return false
    }
    if (!otherCode || otherCode.length !== 3 || otherCode === '+XX') {
        return false
    }
    if (!phone || phone.length !== 10) {
        return false
    }
    return true
}
export function isPasswordValid(password) {
    if (!password || password.length < 6) {
        return false
    }
    return true
}
export function isEmailValid(email) {
    if (!email) {
        return true
    }
    if (email && email.contains('@')) {
        return true
    }
    return false
}

export function isValidResponse(response) {
    return response && response.data &&  response.data.status === 200
}

export function isValidOTP(otp) {
    for(let i = 0; i < otp.length; i++) {
        if(!otp[i].length || otp[i] === "") {
            return false
        }
    }
    return true
}