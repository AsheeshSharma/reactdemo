import { Dimensions , Platform} from 'react-native'

// Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350
const guidelineBaseHeight = 680

function getPercentage(percentage, total) {
  return (percentage / 100 * total)
}
export const isPortrait = (): boolean => {
  const dim = Dimensions.get('screen')
  return dim.height >= dim.width
}
export const isLandscape = () => {
  const dim = Dimensions.get('screen')
  return dim.width >= dim.height
}

export const getHeight = () => Dimensions.get('screen').height
export const getWidth = () => Dimensions.get('screen').width
export const scale = size => getWidth() / guidelineBaseWidth * size
export const verticalScale = size => getHeight() / guidelineBaseHeight * size
export const moderateScale = (size, factor = 0.5) => size + (scale(size) - size) * factor
export const widthPercentage = percentage => scale(getPercentage(percentage, guidelineBaseWidth))
export const heightPercentage = percentage => scale(getPercentage(percentage, guidelineBaseHeight))