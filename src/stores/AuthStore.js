import { observable, action } from 'mobx'
import { RequestWrapper, REQUEST_TYPE } from '../network';
import { isValidResponse } from '../utils'

export class AuthStore {
  userData
  navigation 

  setUserData(userData) {
    this.userData = userData
  }  

  setNavigationObject(navigation) {
    this.navigation = navigation
  }

  getPhoneNumberText() {
    if(this.userData && this.userData.phone) {
      return `We have sent a 4 digit OTP to the number *******${this.userData.phone.substr(this.userData.phone.length - 5)}`
    }
    return 'Uh Oh! Register your details again.'
  }
  
  constructor() {
    this.init()
  }

  @action
  init() {
    //
  }

  async hitSignUpRequest() {
    const net = new RequestWrapper({requestType: REQUEST_TYPE.GET_REQUEST, 
      url: 'http://www.mocky.io/v2/5c682d733800006516b10150'})
    const response = await net.hitNetworkRequest()
    this.validateReponseAndNavigate(response)
  }

  @action
  validateReponseAndNavigate(response) {
    if(isValidResponse(response)) {
      this.navigation.navigate('OTPVerificationPage')
    }
  }
}
