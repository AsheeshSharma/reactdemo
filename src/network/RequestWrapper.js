import axios from 'axios'
/*
    This is just a wrapper with minimum functionality and
    to make a get request.
*/

export const REQUEST_TYPE = {
    GET_REQUEST : '',
    POST_REQUEST: '',
    PUT_REQUEST: '',
    DELETE_REQUEST: ''
}

export class RequestWrapper {
    requestConfig
    constructor({requestType, url, urlParams, postBody, queryParams}) {
        this.requestConfig = {requestType, url, urlParams, postBody, queryParams}
    }
    async hitNetworkRequest() {
        switch(this.requestConfig.requestType) {
            case REQUEST_TYPE.GET_REQUEST:
                return await this.hitGetREquest()
            break
        }
    }

    async hitGetREquest() {
        try {
            const response = await axios.get(this.requestConfig.url);
            return response
        } catch (error) {
            console.warn(error);
        }
    }
}