import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity
} from 'react-native';

import { showSnackbar, colors, isValidOTP } from '../utils'
import { inject, observer } from '../../node_modules/mobx-react';
import { Button, OTPVerificationComponent } from '../components'

@inject('authStore')
@observer
export class OTPVerificationPage extends Component<{}> {
  isCompleted

  constructor(props, state) {
    super(props, state)
    this.state = {
      otp: []
    }
  }

  goBack() {
    // Actions.pop();
  }

  onResendClicked() {
    showSnackbar('OTP sent. Please enter the new OTP.')
  }

  renderResendView() {
    return <View style={styles.signUpTextView}>
      <Text>Otp not recieved ?</Text>
      <TouchableOpacity onPress={() => this.onResendClicked()}>
        <Text style={{ fontWeight: 'bold' }}> Resend</Text>
      </TouchableOpacity>
    </View>
  }

  onOTPFulfilled(otp) {
    this.isCompleted = true
    this.setState({ otp })
  }

  renderDynamicHeader() {
    return <Text style={styles.headerTextStyle}>{this.props.authStore.getPhoneNumberText()}</Text>
  }

  onSubmitOTP() {
    if (this.state.otp && this.state.otp.length === 4 && isValidOTP(this.state.otp) && this.isCompleted) {
      showSnackbar('Processing')
      setTimeout(() => {
        showSnackbar('OTP verified.')
      }, 500)
    } else {
      showSnackbar('Please enter OTP first.')
    }
  }

  renderSubmitButton() {
    return <View style={styles.submitParentView}>
      <Button title={'Submit'} onPress={() => this.onSubmitOTP()} />
    </View>

  }

  render() {
    return (
        <View style={styles.container}>
          {this.renderDynamicHeader()}
          <OTPVerificationComponent onOTPFulfilled={(otp) => this.onOTPFulfilled(otp)} otpLength={4} />
          {this.renderResendView()}
          {this.renderSubmitButton()}
        </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.White,
    flex: 1,
    alignItems: 'center'
  },
  signUpTextView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30
  },
  submitParentView: {
    marginTop: 80
  },
  headerTextStyle: {
    justifyContent: 'center',
    margin: 40,
    textAlign: 'center',
    fontSize: 17
  }
});
