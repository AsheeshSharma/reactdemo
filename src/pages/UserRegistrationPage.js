import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Alert,
  View,
  TouchableWithoutFeedback,
  BackHandler,
  TouchableOpacity
} from 'react-native';
import CheckBox from 'react-native-check-box'
import { inject, observer } from '../../node_modules/mobx-react';

import SplashScreen from 'react-native-splash-screen'
import {InputField,BottomSheet,DialogComponent, Button, FIELD_TYPE } from '../components'
import { TextField } from 'react-native-material-textfield'
import { showSnackbar, COUNTRY_CODE_DATA, OTHER_CODE_DATA, colors, 
  getHeight, widthPercentage, isUserDataValid } from '../utils';

@inject('authStore')
@observer
export class UserRegistrationPage extends Component<{}> {
  passwordRef
  constructor(props, state) {
    super(props, state)
    this.state = {
      password: '',
      email: '',
      phone: '',
      selectedCountryCode: { countryCode: '+XXX' },
      otherCode: { code: '+XX' },
      shouldShowBottomSheet: false,
      checked: false,
      modalVisible: false,
      showPassword: true
    }
    props.authStore.setNavigationObject(props.navigation)
    this.onBackPress = this.onBackPress.bind(this);
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress() {
  }

  async componentDidMount() {
    SplashScreen.hide();
  }

  onSubmitEditing(type) {
    //
  }
  onFocus() {
    //
  }
  onCloseCalled() {
    this.setState({ modalVisible: false })
  }

  continuePressed() {
    const userData = {
      phone: this.state.phone,
      email: this.state.email,
      countryCode: this.state.selectedCountryCode.countryCode,
      otherCode: this.state.otherCode.code,
      password: this.state.password,
      isTCChecked: this.state.checked
    }
    const validation = isUserDataValid(userData)
    const message = validation.message
    showSnackbar(message)
    if(!validation.valid) {
      return
    }
    this.props.authStore.setUserData(userData)
    this.props.authStore.hitSignUpRequest()
    // this.props.navigation.navigate('OTPVerificationPage')
  }

  onSignInPressed() {
    showSnackbar('Not yet implemented! We are working on it.')
  }

  onCountryCodeChanged(selectedCountryCode) {
    this.setState({ selectedCountryCode, shouldShowBottomSheet: !this.state.shouldShowBottomSheet })
  }

  onInputTextChanged(phone) {
    this.setState({ phone })
  }

  renderPhoneField() {
    return <View style={styles.phoneParentStyle}>
      <InputField
        type={FIELD_TYPE.PHONE}
        onInputTextChanged={(phone) => this.onInputTextChanged(phone)}
        countryCode={this.state.selectedCountryCode.countryCode}
        otherCode={this.state.otherCode.code}
        showOtherCode={() => this.setState({ modalVisible: !this.state.modalVisible })}
        showCountryCode={() => this.setState({ shouldShowBottomSheet: !this.state.shouldShowBottomSheet })}
      />
    </View>
  }

  renderPasswordField() {
    return <View style={styles.passwordParentStyle}>
      <TextField
        label='Password'
        inputContainerPadding={8}
        onChangeText={password => this.setState({ password })}
        secureTextEntry={this.state.showPassword}
        autoCorrect={false}
        textColor={colors.Black}
        tintColor={colors.Black}
        baseColor={colors.Black}
        onFocus={() => this.onFocus()}
        blurOnSubmit={true}
        returnKeyType='next'
        ref={x => this.passwordRef = x}
        onSubmitEditing={this.onSubmitEditing.bind(this, 'password')}
        autoCapitalize={'none'}
      />
      <View
        style={styles.togglePasswordStyle}>
        <TouchableWithoutFeedback onPress={() => this.setState({ showPassword: !this.state.showPassword })}>
          <View>
            <Text>Show</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  }

  renderEmailField() {
    return <View style={styles.emailParentStyle}>
      <TextField
        label='Email (Optional)'
        inputContainerPadding={8}
        onChangeText={email => this.setState({ email })}
        autoCorrect={false}
        onFocus={() => this.onFocus()}
        blurOnSubmit={true}
        returnKeyType='next'
        ref='email'
        onSubmitEditing={this.onSubmitEditing.bind(this, 'email')}
        autoCapitalize={'none'}
        clearButtonMode={'while-editing'}
      />
    </View>
  }

  renderTAndCView() {
    return <View style={styles.tAndCParentStyle}>
      <View style={styles.tAndCChildStyle}>
        <CheckBox
          isChecked={this.state.checked}
          onClick={() => this.setState({ checked: !this.state.checked })}
          style={{}}
          checkBoxColor={colors.DeepBlue}
        />
        <View style={{ marginLeft: 3 }}>
          <Text style={styles.tcTextStyle}>By Clicking continue you are greeting to the Noon Pay's</Text>
          <Text style={styles.tcTextStyle}>Terms and Conditions</Text>
        </View>
      </View>
    </View>
  }

  renderSubmitView() {
    return <View style={styles.submitViewStyle}>
      <Button title={'Continue'} onPress={() => this.continuePressed()}/>
      <View style={styles.signUpTextView}>
        <Text>Already have an account?</Text>
        <TouchableOpacity onPress={() => this.onSignInPressed()}>
          <Text style={{ fontWeight: 'bold' }}> Sign In</Text>
        </TouchableOpacity>
      </View>
    </View>
  }

  onDialogItemCliked(item) {
    this.onCloseCalled()
    this.setState({ otherCode: item })
  }

  renderCodeDialog() {
    return <DialogComponent
      modalVisible={this.state.modalVisible}
      data={OTHER_CODE_DATA}
      keyCode={'code'}
      onDialogItemCliked={(item) => this.onDialogItemCliked(item)}
      dialogTitle={'Select other code'}
      onCloseCalled={() => this.onCloseCalled()} />
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderCodeDialog()}
        {this.renderPhoneField()}
        {this.renderPasswordField()}
        {this.renderEmailField()}
        {this.renderTAndCView()}
        {this.renderSubmitView()}
        <BottomSheet data={COUNTRY_CODE_DATA}
          shouldShowBottomSheet={this.state.shouldShowBottomSheet}
          onCountryCodeChanged={(code) => this.onCountryCodeChanged(code)}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.White,
    flex: 1
  }, togglePasswordStyle: {
    position: 'absolute',
    top: 30,
    right: 0,
    opacity: 0.6
  },
  buttonStyle: {
    textAlign: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    fontSize: 14,
    width: 300,
    fontWeight: 'bold', color: colors.White,
    justifyContent: 'center', backgroundColor: colors.ButtonGray
  },
  signUpTextView: {
    flexDirection: 'row', marginTop: 20, justifyContent: 'center'
  },
  submitViewStyle: {
    height: 40,
    position: 'absolute',
    width: widthPercentage(100),
    alignContent: 'center',
    alignItems: 'center',
    top: getHeight() - getHeight() / 3
  },
  tcTextStyle: {
    alignContent: 'center',
    justifyContent: 'center', textAlignVertical: 'center'
  },
  boldStyle: { fontWeight: '600' },
  passwordParentStyle: { marginLeft: 25, marginRight: 25, marginTop: 15 },
  tAndCParentStyle: { marginTop: 25 },
  phoneParentStyle: { marginTop: 30 },
  tAndCChildStyle: { flexDirection: 'row', justifyContent: 'center' },
  emailParentStyle: { marginLeft: 25, marginRight: 25, marginTop: 5 }
});
