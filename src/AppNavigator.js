import { createStackNavigator } from 'react-navigation';
import { UserRegistrationPage, OTPVerificationPage } from './pages';
import { getContainable } from './utils'
const options = {
  header: null,
  gesturesEnabled: false
}

const loginScreen = {
  screen: getContainable(UserRegistrationPage, {
    showHeader: true,
    headerConfiguration: {
      title: 'Sign Up',
      titleConfig: {
          placement: 'center',
          isOrientationRow: true
      },
      iconConfig: {
        isBackIconVisible: false,
        isNotificationIconVisible: false
      }
    }
  }),
  navigationOptions: options
}

const signUpScreen = {
  screen: getContainable(OTPVerificationPage, {
    showHeader: true,
    headerConfiguration: {
      title: 'Verify your mobile',
      titleConfig: {
        placement: 'center',
        isOrientationRow: true
    },
    iconConfig: {
      isBackIconVisible: true,
      isNotificationIconVisible: true
    }
    }
  }),
  navigationOptions: options
}

const AppNavigator = createStackNavigator({
  UserRegistrationPage: loginScreen,
  OTPVerificationPage: signUpScreen
},
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  }
);

export default AppNavigator;