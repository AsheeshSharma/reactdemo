import React, { Component } from 'react';

import {
    TouchableOpacity,
    StyleSheet,
    Text
} from "react-native";
import { colors } from '../utils';

export const Button = ({ title, onPress }) => (
    <TouchableOpacity onPress={() => onPress()}>
        <Text style={styles.buttonStyle}>{title}</Text>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    buttonStyle: {
        textAlign: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        fontSize: 14,
        width: 300,
        fontWeight: 'bold', color: colors.White,
        justifyContent: 'center', backgroundColor: colors.ButtonGray
    }
});