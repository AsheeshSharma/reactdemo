import * as React from 'react';
import ReactNative from 'react-native';
const {
    TextInput,
    View,StyleSheet
} = ReactNative;
import { showSnackbar, widthPercentage, colors } from '../utils';

export class OTPVerificationComponent extends React.Component<> {
    otpRefs = []
    constructor(props, state) {
        super(props, state)
        let tempOTP = []
        for(let i = 0; i < props.otpLength; i++) {
            tempOTP[i] = -1
        }
        this.state = {
            otp: tempOTP
        }
    }

    onFocus(index) {
        let firstEmptyIndex = -1
        for (let i = 0; i < index; i++) {
            if (isNaN(this.state.otp[i]) || this.state.otp[i] === -1 || !this.state.otp[i].length) {
                firstEmptyIndex = i;
                break
            }
        }
        if (firstEmptyIndex != -1) {
            this.otpRefs[firstEmptyIndex].focus()
        }
    }

    onNumberEntered(index, number) {
        let arr = this.state.otp
        if (number.length > 1 && index < this.props.otpLength - 1) {
            arr[index + 1] = "" + number % 10
            this.otpRefs[index + 1].focus()
        } else {
            arr[index] = number
        }
        this.setState({ otp: arr }, function () {
            if (!number.length && index > 0) {
                this.otpRefs[index - 1].focus()
                return
            }
            if (index < this.props.otpLength - 1) {
                if (index === 0 && this.state.otp[index] === "") {
                    return
                }
                this.otpRefs[index + 1].focus()
            }
            if (index === this.props.otpLength - 1 && number.length && number !== -1) {
                this.props.onOTPFulfilled(this.state.otp)
            }
        })

    }

    onSubmitEditing(type) {
       //
    }

    onkeyPressed(index) {
        if (isNaN(this.state.otp[index])) {
            showSnackbar('Enter a correct number!')
            this.otpRefs[index].focus()
            return
        }
        if (index < this.props.otpLength - 1) {
            this.otpRefs[index + 1].focus()
        }
    }

    renderOTPView() {
        let otpViews = []
        for (let i = 0; i < this.props.otpLength; i++) {
            otpViews.push(<TextInput
                onChangeText={number => this.onNumberEntered(i, number)}
                value={this.state.otp[0] !== -1 ? this.state.otp[i] : ''}
                returnKeyType={i === this.props.otpLength - 1 ? 'done' : 'numeric'}
                onFocus={() => this.onFocus(i)}
                keyboardType={'numeric'}
                maxLength={i === this.props.otpLength - 1 ? 1 : 2}
                ref={x => this.otpRefs[i] = x}
                onSubmitEditing={() => this.onkeyPressed(i)}
                style={styles.otpInputStyle}
            />)
        }


        return <View style={styles.otpParentView}>
            <View style={styles.otpChildView}>
                {otpViews}
            </View>
        </View>
    }


    render() {
        return (
            <View style={{}}>
                {this.renderOTPView()}
            </View>
        );
    }

}

const styles = StyleSheet.create({
    otpParentView: {
        marginTop: 100, marginLeft: 10, marginRight: 10, alignItems: 'center', alignContent: 'center', alignSelf: 'center' 
    },
    otpChildView: {
        flexDirection: 'row'
    },
    otpInputStyle: {
        height: 50, width: widthPercentage(20), fontSize: 15, color: colors.Black
    }
  })