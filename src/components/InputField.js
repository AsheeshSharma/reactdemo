import React, { Component } from 'react';
import {
    StyleSheet, Text, TouchableOpacity, TextInput,
    View,
} from 'react-native';
import { dimens, colors, verticalScale, widthPercentage } from '../utils'

export const FIELD_TYPE = {
    PHONE: 'phone',
    PHONE_WIHTOUT_CODE: 'phone_without'
}

export class InputField extends Component<{}> {

    constructor(props, state) {
        super(props, state)
        this.state = {
            countryCode: props.countryCode,
            otherCode: props.otherCode,
            phone: ''
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.countryCode !== this.props.countryCode) {
            this.setState({countryCode: nextProps.countryCode})
        }
        if(nextProps.otherCode !== this.props.otherCode) {
            this.setState({otherCode: nextProps.otherCode})
        }
    }
    onChangeText(phone) {
        this.setState({phone})
        this.props.onInputTextChanged(phone)
    }


    renderFieldView() {
        return <View style={{  marginLeft : 25, marginRight: 25, justifyContent: 'center'}}>
            <Text style={styles.phoneTextStyle}>Phone Number</Text>
            <View style={{ flexDirection: 'row', 
            alignItems: 'flex-start', borderBottomWidth: 1, borderColor: colors.Black }}>
                <View style={{ alignItems: 'flex-start', justifyContent: 'center' }}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.showCountryCode() }>
                        <View
                            style={[styles.classdropdownView]}>
                            <View style={{ justifyContent: 'center'}}>
                                <Text style={styles.classdropdownText}>{this.state.countryCode}</Text>
                            </View>
                            <View style={styles.triangle} />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ alignItems: 'flex-start', justifyContent: 'center', marginLeft: 10 }}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.showOtherCode()}>
                        <View
                            style={[styles.classdropdownView]}>
                            <View>
                                <Text style={styles.classdropdownText}>{this.state.otherCode}</Text>
                            </View>
                            <View style={styles.triangle} />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ alignItems: 'flex-start', marginLeft: 10 }}>
                    <TextInput
                        underlineColorAndroid='transparent'
                        onChangeText={(phone) => this.onChangeText(phone)}
                        placeholder={'Phone'}
                        value={this.state.phone}
                        keyboardType={'numeric'}
                        placeholderTextColor={colors.Black}
                        maxLength={10}
                        style={{
                            width: 200,
                            fontSize: 16,
                            color: colors.Black
                        }}
                    />
                </View>
            </View>
        </View>
    }

    renderField() {
        const type = this.props.type
        if (type) {
            switch (type) {
                case FIELD_TYPE.PHONE:
                    return this.renderFieldView()
                    break;
                case FIELD_TYPE.PHONE_WIHTOUT_CODE:
                    break;
                default:
                    break;
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderField()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        
    }, classdropdownView: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    congText: {
        fontSize: 16,
        color: colors.LightBlue
    },
    phoneTextStyle: {
        width: 200,
        fontSize: 14
    },
    classdropdownText: {
        fontSize: 16,
        color: colors.Black
    }, togglePasswordStyle: {
        position: 'absolute',
        top: 30,
        right: 0,
        opacity: 0.6
    },
    triangle: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderRightWidth: 5,
        borderBottomWidth: 5,
        alignItems: 'flex-end',
        alignContent: 'flex-end',
        alignSelf: 'center',
        borderLeftWidth: 5,
        marginTop: 1,
        marginLeft: 7,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: colors.Black,
        borderLeftColor: 'transparent',
        transform: [{ rotate: '180deg' }],
        zIndex: 9
    },
    loginButtonView: {
        backgroundColor: colors.LightBlue,
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: verticalScale(85),
        width: widthPercentage(74),
        height: 45
        // fontSize: isPortrait() ? verticalScale(15) : widthPercentage(3)
    },
    radioStyle: {
        marginRight: 0,
        marginLeft: 0,
        width: 35,
        height: 20,
        backgroundColor: colors.Transparent,
        borderWidth: 0,
        marginTop: 0
    }, textTitle: {
        fontSize: dimens.size17,
        color: colors.Black,
        paddingLeft: 0
    }, modal: {
        position: 'absolute',
        backgroundColor: colors.Red,
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    },
    fileselectButton: {
        flex: 1,
        position: 'relative',
        backgroundColor: '#fbfbfb'
    },
    tabBorderStyle: {
        marginLeft: 20,
        marginRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
        justifyContent: 'flex-start',
        borderBottomColor: colors.GrayUnderLine,
        borderBottomWidth: 1,
        flex: 1,
        flexDirection: 'row'
    },
    cancelView: {
        backgroundColor: colors.White,
        flex: 1,
        borderRadius: 15,
        marginTop: 15,
        justifyContent: 'center',
        margin: 5,
        padding: 20
    },
    overlayView: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        opacity: 0.6,
        backgroundColor: colors.GrayUnderLine, zIndex: 9
    },
    hideOverlayView: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        zIndex: 10
    },
    buttonView: {
        position: 'absolute',
        left: 10,
        bottom: 20,
        width: widthPercentage(100) - 20,
        zIndex: 11
    },
    buttonViewInner: {
        backgroundColor: colors.White,
        flex: 1,
        borderRadius: 20,
        justifyContent: 'center',
        margin: 5
    }, radioStyleCountry: {
        marginRight: 5,
        backgroundColor: colors.Transparent,
        borderWidth: 0
    }
});