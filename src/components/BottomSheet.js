import * as React from 'react';
import ReactNative from 'react-native';
const {
    StyleSheet,
    Text,
    View,
    Animated,
    FlatList,
    Image
} = ReactNative;
import { colors } from '../utils';
import CheckBox from 'react-native-check-box'

var isHidden = true;
// TODO Asheesh: It has to be dynamic by calculating height on runtime/length of items
export class BottomSheet extends React.Component<> {
    constructor(props, state) {
        super(props, state)
        this.state = {
            bounceValue: new Animated.Value(250),  //This is the initial position of the subview
            selectedCountryCode: {}
        };
    }

    _toggleSubview() {
        var toValue = 250;
        if (isHidden) {
            toValue = 0;
        }
        Animated.spring(
            this.state.bounceValue,
            {
                toValue: toValue,
                velocity: 10,
                tension: 2,
                friction: 10,
            }
        ).start();

        isHidden = !isHidden;
    }

    onCountryCodeSelected(type) {
        this.props.onCountryCodeChanged(type)
        this.setState({ selectedCountryCode: type })
    }

    renderBottomModelItemView(type) {
        return <View style={[styles.tabBorderStyle, { borderWidth: 1, borderColor: colors.GrayCalm }]} key={type.countryCode}>
            <View style={styles.otherView}>
                <View style={styles.alignCentreCommon}>
                    <Image
                        style={styles.imageView}
                        source={type.image}
                    />
                </View>
                <View style={[styles.alignCentreCommon]}>
                    <Text style={styles.codeStyle}>{type.countryCode}</Text>
                </View>
                <View style={[styles.alignCentreCommon, {marginLeft: 5 }]}>
                    <Text style={styles.codeStyle}>{type.countryName}</Text>
                </View>
            </View>
            <View style={styles.checkBoxContainer}>
                <CheckBox
                    isChecked={type.countryCode === this.state.selectedCountryCode.countryCode}
                    onClick={() => this.onCountryCodeSelected(type)}
                    style={{}}
                    checkBoxColor={colors.DeepBlue}
                />
            </View>
        </View>
    }

    getItemExtractor(item, index) {
        return index
    }

    render() {
        isHidden = this.props.shouldShowBottomSheet
        this._toggleSubview()
            return (
                    <Animated.View
                        style={[styles.subView,
                        { transform: [{ translateY: this.state.bounceValue }] }]}
                    >
                        <FlatList
                            extraData={this.state.selectedCountryCode}
                            data={this.props.data}
                            keyExtractor={(item, index) => this.getItemExtractor(item, index)}
                            renderItem={({ item }) => this.renderBottomModelItemView(item)}
                        />
                    </Animated.View>
            );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        marginLeft: 10, marginRight: 10,
        backgroundColor: colors.Transparent
    },
    button: {
        padding: 8,
    },
    buttonText: {
        fontSize: 17,
        color: "#007AFF"
    },
    subView: {
        position: "absolute",
        borderColor: colors.ButtonGray,
        borderWidth: 1,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#FFFFFF",
        flex: 1,
    }, tabBorderStyle: {
        marginLeft: 20,
        marginRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
        justifyContent: 'flex-start',
        borderBottomColor: colors.GrayUnderLine,
        borderBottomWidth: 1,
        flex: 1,
        flexDirection: 'row'
    },
    otherView: {
        flexDirection: 'row', 
        flex: 0.8
    },imageView: { width: 30, height: 30, marginRight: 10 },
    codeStyle:{ textAlign: 'left', fontSize: 14 },
    alignCentreCommon:{ justifyContent: 'center' },
    checkBoxContainer:{flexDirection: 'row', flex: 0.2, justifyContent: 'center', alignItems: 'flex-end', alignSelf: 'flex-end'}
});
