import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image
} from 'react-native';
import { icons, colors } from '../utils';

export class HeaderComponent extends Component<{}> {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props)
  }

  getHeaderBackIcon() {
    const config = this.props.iconConfig
    if (config) {
      if (config.isBackIconVisible) {
        return <View style={styles.backIconContainerStyle}>
          <TouchableOpacity onPress={() => this.props.onBackPressed()}>
            <Image style={styles.backIconStyle}
              source={icons.BACK_ICON} />
          </TouchableOpacity></View>
      }
      return null
    }
    return null
  }

  getNotificationIcon() {
    const config = this.props.iconConfig
    if (config) {
      if (config.isNotificationIconVisible) {
        return <Image style={styles.notificationIconStyle}
          source={icons.NOTIFICATION_ICON} />
      }
      return null
    }
    return null
  }

  getLeftOrientationTitleText() {
    const config = this.props.iconConfig
    if (config) {
      if (config.isBackIconVisible) {
        return <View style={styles.textViewContainerLeftStyle}>
          <Text style={styles.headerTitleStyle}>{this.props.title}</Text>
        </View>
      }
    }
    return <View style={styles.textViewContainerLeftStyleNoIcon}>
      <Text style={styles.headerTitleStyle}>{this.props.title}</Text>
    </View>
  }

  renderHeaderTitleText() {
    if (this.props.titleConfig) {
      const config = this.props.titleConfig
      const parentStyle = config.isOrientationRow ? styles.childContainerView : styles.childContainerViewColumn
      if (config && config.placement) {
        switch (config.placement) {
          case 'center':
            return <View style={parentStyle}>
              {this.getHeaderBackIcon()}
              <View style={styles.textViewContainerStyle}>
                <Text style={styles.headerTitleStyle}>{this.props.title}</Text>
              </View>
              {this.getNotificationIcon()}
            </View>
            break;
          case 'left':
            return <View style={parentStyle}>
              {this.getHeaderBackIcon()}
              {this.getLeftOrientationTitleText()}
              {this.getNotificationIcon()}
            </View>
            break;
          default:
            break;
        }

      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderHeaderTitleText()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.YellowTheme,
    height: 66,
    justifyContent: 'center'
  },
  childContainerView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center'
  }, childContainerViewColumn: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'center'
  },
  backIconStyle: {
    width: 25,
    height: 25
  },
  backIconContainerStyle: {
    width: 25,
    height: 25,
    position: 'absolute',
    top: 20,
    left: 20
  },
  notificationIconStyle: {
    width: 25,
    height: 25,
    position: 'absolute',
    top: 20,
    right: 20
  },
  textViewContainerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  textViewContainerLeftStyle: {
    flex: 1,
    marginLeft: 60,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'flex-start'
  },
  textViewContainerLeftStyleNoIcon: {
    flex: 1,
    marginLeft: 20,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'flex-start'
  },
  headerTitleStyle: {
    fontSize: 22,
    fontWeight: 'bold',
    justifyContent: 'center',
    color: 'black',
    textAlign: 'center',
    textAlignVertical: 'center',
    flex: 1
  }
});
