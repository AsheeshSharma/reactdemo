import React, { Component } from 'react';
import {
    Modal, Text, FlatList, StyleSheet, TouchableWithoutFeedback,
    View, Alert
} from 'react-native';
import { colors, dimens } from '../utils';

// TODO Currently it is just a list based modal otherwise
// on the basis of type it can be made geneirc.
export class DialogComponent extends Component {

    constructor(props, state) {
        super(props, state)
        this.state = {
            modalVisible: props.modalVisible,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.modalVisible !== this.props.modalVisible) {
            this.setState({ modalVisible: nextProps.modalVisible })
        }
    }


    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
        this.props.onCloseCalled()
    }

    onDialogItemCliked(item) {
        if(this.props.onDialogItemCliked) {
            this.props.onDialogItemCliked(item)
        }
    }

    renderBottomModelItemView(item, keyCode) {
        return <TouchableWithoutFeedback style={{ flex: 1 }} onPress={() => this.onDialogItemCliked(item)}>
            <View style={styles.parentItemStyle} key={item[keyCode]}>
                <View style={[styles.alignCentreCommon, { marginLeft: 5 }]}>
                    <Text style={styles.textTitle}>{item[keyCode]}</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    }

    renderListItems() {
        const key = this.props.keyCode
        return <FlatList
            extraData={this.state.selectedCountryCode}
            data={this.props.data}
            renderItem={({ item }) => this.renderBottomModelItemView(item, key)}
        />
    }

    render() {
        if (this.props.modalVisible) {
            return (
                <View>
                    <TouchableWithoutFeedback onPress={() => this.setModalVisible(false)}>
                        <Modal
                            animationType="slide"
                            transparent={false}
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                this.setModalVisible(false)
                            }}>
                            <TouchableWithoutFeedback onPress={() => this.setModalVisible(false)}>
                                <View style={styles.parentContainer}>
                                    <View style={styles.childContainer}>
                                        <View style={styles.viewContainer}>
                                            {/* tslint:disable-next-line:no-empty */}
                                            <TouchableWithoutFeedback onPress={() => { }}>
                                                <View style={styles.backbuttonStyle}>
                                                    <View>
                                                        <Text style={styles.headerStyle}>{this.props.dialogTitle ? this.props.dialogTitle : 'Choose'}</Text>
                                                    </View>
                                                </View>
                                            </TouchableWithoutFeedback>
                                            {this.renderListItems()}
                                        </View>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    </TouchableWithoutFeedback>
                </View>
            );
        }
        return null
    }
}

const styles = StyleSheet.create({
    viewContainer: {
        paddingTop: 20,
        paddingBottom: 40
    },
    childContainer: {
        marginLeft: 40,
        marginRight: 40,
        paddingBottom: 40,
        paddingTop: 10,
        justifyContent: 'center',
        flex: 1
    },
    parentContainer: {
        backgroundColor: colors.WhiteColorOpacity75,
        flex: 1
    },
    headerStyle: {
        alignSelf: 'flex-start',
        fontSize: 15,
        backgroundColor: colors.GrayTitle,
        paddingTop: 18,
        paddingBottom: 18
    }, modal: {
        backgroundColor: 'white',
        margin: 30,
        alignItems: undefined,
        justifyContent: undefined,
    },
    backbuttonStyle: {
        flexDirection: 'row',
        position: 'relative',
        paddingLeft: 15,
        backgroundColor: colors.GrayTitle
    },
    textTitle: {
        fontSize: dimens.size16,
        color: colors.Black,
        alignSelf: 'flex-start',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 5
    },
    parentItemStyle: {
        backgroundColor: colors.White,
        marginTop: 1,
        marginBottom: 1
    },
});


