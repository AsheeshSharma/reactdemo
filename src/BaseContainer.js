import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import { HeaderComponent } from './components';

export class BaseContainer extends Component<> {
  constructor(props, state) {
    super(props, state)
    const { children } = props
    let concreteContainer
    if (children && children.length) {
      [concreteContainer] = children
    } else if (children) {
      concreteContainer = children
    }
    this.state = {
      concreteContainer: concreteContainer
    }
  }

  render() {
    const { concreteContainer } = this.state
    console.disableYellowBox = true; 
    if (concreteContainer) {
      return <View style={styles.container}>
        {this.renderPage()}
      </View>
    }
    return undefined
  }

  renderPage() {
    const { concreteContainer } = this.state
    return <View
      style={{ flex: 1 }}>
      {this.renderHeaderComponent()}
      {concreteContainer}
    </View>
  }

  onBackPressed() {
    this.props.navigation.goBack()
  }

  renderHeaderComponent (){
      if(this.props.configuration && this.props.configuration.showHeader) {
          return <HeaderComponent
          title={this.props.configuration.headerConfiguration.title}
          titleConfig={this.props.configuration.headerConfiguration.titleConfig}
          iconConfig={this.props.configuration.headerConfiguration.iconConfig}
          onBackPressed={() => this.onBackPressed()}
        />
      }
      return null
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})